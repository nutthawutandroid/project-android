package com.project.incomeandexpenses

import android.content.Context
import android.inputmethodservice.InputMethodService
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.widget.addTextChangedListener
import androidx.room.Room
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.addtransaction_activity.*
import kotlinx.android.synthetic.main.addtransaction_activity.Closebutton
import kotlinx.android.synthetic.main.addtransaction_activity.amountInput
import kotlinx.android.synthetic.main.addtransaction_activity.amountLayout
import kotlinx.android.synthetic.main.addtransaction_activity.descripInput
import kotlinx.android.synthetic.main.addtransaction_activity.labelInput
import kotlinx.android.synthetic.main.addtransaction_activity.labelLayout
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class DetailActivity : AppCompatActivity() {
    private lateinit var transaction : Transaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        transaction = intent.getSerializableExtra("transactions") as Transaction

        labelInput.setText(transaction.label)
        amountInput.setText(transaction.amount.toString())
        descripInput.setText(transaction.description)

        rootView.setOnClickListener{
            this.window.decorView.clearFocus()

            val hidekeyboard = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            hidekeyboard.hideSoftInputFromWindow(it.windowToken, 0)
        }

        labelInput.addTextChangedListener {
            ButtonUpdate.visibility = View.VISIBLE
            if (it!!.isNotEmpty())
                labelLayout.error = null
        }

        amountInput.addTextChangedListener {
            ButtonUpdate.visibility = View.VISIBLE
            if (it!!.isNotEmpty())
                amountLayout.error = null
        }

        descripInput.addTextChangedListener {
            ButtonUpdate.visibility = View.VISIBLE
        }

        ButtonUpdate.setOnClickListener {
            val label: String = labelInput.text.toString()
            val description: String = descripInput.text.toString()
            val amount: Double? = amountInput.text.toString().toDoubleOrNull()

            if (label.isEmpty())
                labelLayout.error = "กรุณากรอกหัวข้อเรื่อง"
            else if (amount == null)
                amountLayout.error = "กรุณากรอกจำนวนเงิน"
            else {
                val transaction = Transaction(transaction.id, label, amount, description)
                update(transaction)
            }

        }

        Closebutton.setOnClickListener {
            finish() // Click ปิดก็จะปิดเลย
        }
    }

    private fun update(transactions: Transaction) {
        val db = Room.databaseBuilder(this, AppDatabase::class.java, "transactions").build()

        GlobalScope.launch {
            db.transactionDao().update(transactions)
            finish()
        }
    }
}
