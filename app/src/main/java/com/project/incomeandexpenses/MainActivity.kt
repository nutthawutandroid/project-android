package com.project.incomeandexpenses

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private lateinit var deleteTransaction: Transaction
    private lateinit var transactionsmain: List<Transaction>
    private lateinit var oldtransaction: List<Transaction>
    private lateinit var transactionAdaptermain: TransactionAdapter
    private lateinit var linearlayoutManager: LinearLayoutManager
    private lateinit var db: AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        transactionsmain = arrayListOf()

        transactionAdaptermain = TransactionAdapter(transactionsmain)
        linearlayoutManager = LinearLayoutManager(this)

        db = Room.databaseBuilder(this, AppDatabase::class.java, "transactions").build()

        recyclerview.apply {
            adapter = transactionAdaptermain
            layoutManager = linearlayoutManager
        }

        // Swipe to remove
        val itemTouchHelper = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                deleteTransaction(transactionsmain[viewHolder.adapterPosition])
            }
        }

        val swipeHelper = ItemTouchHelper(itemTouchHelper)
        swipeHelper.attachToRecyclerView(recyclerview)

        AddTransactionsBtn.setOnClickListener {
            val intent = Intent(this, AddtransactionActivity::class.java)
            startActivity(intent)
        }
    }


    private fun fetchAll() {
        GlobalScope.launch {
            transactionsmain = db.transactionDao().getAll()
            runOnUiThread {
                updateDashboard()
                transactionAdaptermain.setData(transactionsmain)
            }
        }
    }

    private fun updateDashboard() {
        val totalamount: Double = transactionsmain.sumOf { it.amount }
        val budgetamount: Double = transactionsmain.filter { it.amount > 0 }.sumOf { it.amount }
        val expenseamount: Double = totalamount - budgetamount

        balance.text = "%.2f ฿".format(totalamount)
        budget.text = "+%.2f ฿".format(budgetamount)
        expense.text = "%.2f ฿".format(expenseamount)
    }

    private fun undoDelete() {
        GlobalScope.launch {
            db.transactionDao().insertAll(deleteTransaction)

            transactionsmain = oldtransaction

            runOnUiThread {
                transactionAdaptermain.setData(transactionsmain) // set ไว้เพื่อตอนยกเลิกการลบรายการจะกลับมา
                updateDashboard()
            }
        }
    }

    private fun showSnacbar() { // Delete Snackbar
        val view = findViewById<View>(R.id.coordinator)
        val snacbar = Snackbar.make(view, "รายการธุรกรรมกำลังถูกลบ", Snackbar.ANIMATION_MODE_SLIDE)
        snacbar.setAction("ยกเลิกการลบ") {
            undoDelete()
        }
            .setActionTextColor(ContextCompat.getColor(this, R.color.teal_200))
            .setTextColor(ContextCompat.getColor(this, R.color.white))
            .show()
    }

    private fun deleteTransaction(transaction: Transaction) {
        deleteTransaction = transaction
        oldtransaction = transactionsmain

        GlobalScope.launch {
            db.transactionDao().delete(transaction)

            transactionsmain = transactionsmain.filter {
                it.id != transaction.id
            }
            runOnUiThread {
                updateDashboard()
                transactionAdaptermain.setData(transactionsmain)// set ไว้เพื่อตอนยกเลิกการลบรายการจะกลับมา
                showSnacbar()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        fetchAll()
    }

}

