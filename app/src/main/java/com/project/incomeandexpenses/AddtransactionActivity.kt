package com.project.incomeandexpenses

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.widget.addTextChangedListener
import androidx.room.Room
import kotlinx.android.synthetic.main.addtransaction_activity.*
import kotlinx.android.synthetic.main.addtransaction_activity.Closebutton
import kotlinx.android.synthetic.main.addtransaction_activity.amountInput
import kotlinx.android.synthetic.main.addtransaction_activity.amountLayout
import kotlinx.android.synthetic.main.addtransaction_activity.descripInput
import kotlinx.android.synthetic.main.addtransaction_activity.labelInput
import kotlinx.android.synthetic.main.addtransaction_activity.labelLayout
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class AddtransactionActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.addtransaction_activity) // set layout เพื่อให้มาแสดงถูกคลาส

        labelInput.addTextChangedListener {
            if (it!!.isNotEmpty())
                labelLayout.error = null
        }
        amountInput.addTextChangedListener {
            if (it!!.isNotEmpty())
                amountLayout.error = null
        }

        ButtonAddTransaction.setOnClickListener {
            val label: String = labelInput.text.toString()
            val description: String = descripInput.text.toString()
            val amount: Double? = amountInput.text.toString().toDoubleOrNull()

            if (label.isEmpty())
                labelLayout.error = "กรุณากรอกหัวข้อเรื่อง"
            else if (amount == null)
                amountLayout.error = "กรุณากรอกจำนวนเงิน"
            else {
                val transaction = Transaction(0, label, amount, description)
                insert(transaction)
            }
        }

        Closebutton.setOnClickListener {
            finish() // Click ปิดก็จะปิดเลย
        }
    }

    private fun insert(transaction: Transaction) {
        val db = Room.databaseBuilder(this, AppDatabase::class.java, "transactions").build()

        GlobalScope.launch {
            db.transactionDao().insertAll(transaction)
            finish()
        }
    }
}